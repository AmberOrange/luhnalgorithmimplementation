##Luhn Algorithm implementation
A Test-Driven Development (TDD) of the Luhn algorithm as described on [Wikipedia](https://en.wikipedia.org/wiki/Luhn_algorithm).
The project uses Java14, Gradle and JUnit5.