package se.experis;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.util.InputMismatchException;

import static org.junit.jupiter.api.Assertions.*;

class ProgramTest {

    Luhn luhn;

    @BeforeEach
    void setUp() {
        luhn = new Luhn();
    }

    @Test
    void nothing() {
    }

    @Test
    void inputValidDigit() {
        // A way to hi-jack the system input stream for testing purposes
        System.setIn(new ByteArrayInputStream("20".getBytes()));
        luhn.setUserNumberFromSystemInput();
        assertEquals(20, luhn.getUserNumber());

        System.setIn(new ByteArrayInputStream(Long.toString(Long.MAX_VALUE).getBytes()));
        luhn.setUserNumberFromSystemInput();
        assertEquals(Long.MAX_VALUE, luhn.getUserNumber());
    }

    @Test
    void inputMismatchException() {
        // A way to hi-jack the system input stream for testing purposes
        System.setIn(new ByteArrayInputStream("Greg's".getBytes()));
        assertThrows(InputMismatchException.class,
                () -> luhn.setUserNumberFromSystemInput());
    }

    @Test
    void getFormattedInput() {
        luhn.setUserNumber(123456789);
        assertEquals("12345678 9", luhn.getFormattedInput());

        luhn.setUserNumber(98765432123456789L);
        assertEquals("9876543212345678 9", luhn.getFormattedInput());
    }

    @Test
    void checkValiditySuccess() {
        luhn.setUserNumber(4242424242424242L);
        assertTrue(luhn.isValid(),
                String.format("userNumber: %d, checkDigit: %d, expectedDigit: %d",
                    luhn.getUserNumber(),
                    luhn.getCheckDigit(),
                    luhn.getExpectedDigit()));
    }

    @Test
    void checkValidityFailure() {
        luhn.setUserNumber(1337);
        assertFalse(luhn.isValid(),
                String.format("userNumber: %d, checkDigit: %d, expectedDigit: %d",
                        luhn.getUserNumber(),
                        luhn.getCheckDigit(),
                        luhn.getExpectedDigit()));
    }

    @Test
    void testLuhnConstructors() {
        luhn = new Luhn();
        assertEquals(0, luhn.getUserNumber());
        assertEquals(0, luhn.getCheckDigit());
        assertEquals(0, luhn.getExpectedDigit());
        assertFalse(luhn.isValid());
        assertEquals("", luhn.getFormattedInput());

        luhn = new Luhn(57842304);
        assertEquals(57842304, luhn.getUserNumber());
        assertEquals(4, luhn.getCheckDigit());
        assertEquals(4, luhn.getExpectedDigit());
        assertTrue(luhn.isValid());
        assertEquals("5784230 4", luhn.getFormattedInput());
    }

    @Test
    void getCreditCardEligibilityString() {
        luhn.setUserNumber(5212_5231_2512_3212L);
        assertEquals("16 (credit card)", luhn.getCreditCardEligibilityString());

        luhn.setUserNumber(5212_5231_2512_32L);
        assertEquals("14", luhn.getCreditCardEligibilityString());
    }

    @Test
    void tryAOneDigitNumber() {
        luhn.setUserNumber(2);
        assertEquals(2, luhn.getUserNumber());
        assertEquals(2, luhn.getCheckDigit());
        assertEquals(0, luhn.getExpectedDigit());
        // A one number digit can never be valid (according to my interpretation)
        assertFalse(luhn.isValid());
        assertEquals("2", luhn.getFormattedInput());
    }
}