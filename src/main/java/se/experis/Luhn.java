package se.experis;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Luhn {
    private long userNumber;
    private byte checkDigit;
    private byte expectedDigit;
    private byte digitCount;

    public Luhn() {
    }

    public Luhn(long userNumber) {
        this.userNumber = userNumber;
        validateNumber();
    }

    public void setUserNumber(long userNumber) {
        this.userNumber = userNumber;
        validateNumber();
    }

    // Gets the next "long" integer from the user
    public void setUserNumberFromSystemInput() throws InputMismatchException {
        Scanner scanner = new Scanner(System.in);
        userNumber = scanner.nextLong();
        validateNumber();
    }

    public long getUserNumber() {
        return userNumber;
    }

    public byte getCheckDigit() {
        return checkDigit;
    }

    public byte getExpectedDigit() {
        return expectedDigit;
    }

    // Implements the luhn algorithm as described here:
    // https://en.wikipedia.org/wiki/Luhn_algorithm
    //
    // This function is called whenever the user number gets updated
    private void validateNumber() {
        long number = userNumber;
        checkDigit = (byte)(number % 10);
        byte factor = 1;
        int sumOfDigits = 0;
        digitCount = 1;
        while((number /= 10) != 0) {
            byte currentDigit = (byte)(number % 10 * ++factor);
            factor %= 2;
            if(currentDigit >= 9) {
                currentDigit -= 9;
            }
            sumOfDigits += currentDigit;
            digitCount++;
        }
        expectedDigit = (byte)((10 - sumOfDigits % 10) % 10);
    }

    public boolean isValid() {
        return userNumber >= 10 && checkDigit == expectedDigit;
    }

    public String getFormattedInput() {
        return userNumber >= 10 ? String.format("%d %d",userNumber / 10, checkDigit) :
                userNumber != 0 ? Long.toString(userNumber) : "";
    }

    public String getCreditCardEligibilityString() {
        return String.format("%d%s",
                digitCount,
                digitCount == 16 ? " (credit card)" : ""
                );
    }
}
