package se.experis;

import java.util.InputMismatchException;

public class Program {
    public static void main(String[] args) {
        Luhn luhn = new Luhn();

        System.out.println("Provide a digit to check: ");
        System.out.print(">");
        while(true)
        {
            try {
                luhn.setUserNumberFromSystemInput();
                break;
            } catch (InputMismatchException e) {
                System.out.println("Input a valid digit.");
                System.out.print(">");
            }
        }

        System.out.printf("Input: %s%n", luhn.getFormattedInput());
        System.out.printf("Provided: %d%n", luhn.getCheckDigit());
        System.out.printf("Expected: %d%n%n", luhn.getExpectedDigit());
        System.out.printf("Checksum: %s%n", luhn.isValid() ? "Valid" : "Invalid");
        System.out.printf("Digits: %s%n", luhn.getCreditCardEligibilityString());

    }
}
